import { Module} from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'

import { ChatGPTService } from 'src/infrastructure/chat-gpt/chat-gpt.service'

@Module({
  imports: [HttpModule],
  providers: [ChatGPTService],
  exports: [ChatGPTService],
})
export class ChatGPTModule {}