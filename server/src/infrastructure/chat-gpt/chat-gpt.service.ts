import { Injectable, Logger} from '@nestjs/common'
import { AxiosResponse } from 'axios'
import { Observable } from 'rxjs'
import { HttpService } from '@nestjs/axios'

export interface ChatGPTMessage {
  role: 'system' | 'user' | 'assistant'
  content: string
}

export interface ChatGPTRequestData {
  model: 'gpt-3.5-turbo'
  temperature: number
  messages: ChatGPTMessage[]
}

@Injectable()
export class ChatGPTService {
  private readonly logger =  new Logger(ChatGPTService.name)
  private readonly apiKey: string
  private readonly apiUrl: string

  constructor(private readonly httpService: HttpService) {
    this.apiKey = process.env.OPENAI_API_KEY
    this.apiUrl = 'https://api.openai.com/v1/chat/completions'
  }

  // TODO type the response
  request(data: ChatGPTRequestData): Observable<AxiosResponse> {
    this.logger.debug(data)
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.apiKey}`,
    };

    return this.httpService.post(this.apiUrl, data, { headers: headers });
  }
}