import { Injectable } from '@nestjs/common'
import { AxiosResponse } from 'axios'
import { map } from 'rxjs'

import { ChatGPTService } from 'src/infrastructure/chat-gpt/chat-gpt.service'

@Injectable()
export class AppService {
    constructor(private readonly _chatGPTService: ChatGPTService) {}

    getWtw(prompt: string) {
        return this._chatGPTService.request({
            model: 'gpt-3.5-turbo',
            temperature: 1.0,
            messages: [
                {
                    role: 'system',
                    content: `
                        The content provided comes from a long description of the weather provided by NOAA.
                        The context is that you should provide a short description of what to wear, in terms of clothing, based on the provided text.
                        The provided text should be reasonably short, so it fits on a mobile screen.
                        Feel free to add additional suggestions. For example, sunscreen on a high UV index day.
                    `
                },
                { role: 'user', content: prompt },
            ]
        }).pipe(map((response: AxiosResponse) => response.data.choices))
    }

    getWtd(prompt: string, region: string, timeframe: string) {
        return this._chatGPTService.request({
            model: 'gpt-3.5-turbo',
            temperature: 1.0,
            messages: [
                {
                    role: 'system',
                    content: `
                        The content provided comes from a long description of the weather provided by NOAA.
                        The region and time of day (time-frame) are also provided.
                        The context is that you should provide a short list if activities based on the weather, region, and time-frame.
                        If this region not a region you recognize, only generate generic results that mostly apply to suburbs in the United States.
                        If you do recognize the region, feel free to throw region-specific sights and sounds.
                        If you do not recognize the time-frame, try to use the weather description to get the time and otherwise make the activities generic enough for any time-frame, based on the weather description.
                        Generate between 5 - 10 activities.
                        The provided text should be reasonably short, so it fits on a mobile screen.
                        Feel free to add additional suggestions about the weather. For example, sunscreen on a high UV index day.
                    `
                },
                { role: 'user', content: prompt },
                { role: 'user', content: `The region is ${region}` },
                { role: 'user', content: `The time-frame is ${timeframe}` },
            ]
        }).pipe(map((response: AxiosResponse) => response.data.choices))
    }

}