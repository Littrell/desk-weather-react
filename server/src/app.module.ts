import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { AppController } from 'src/app.controller'
import { AppService } from 'src/app.service'
import { ChatGPTModule } from 'src/infrastructure/chat-gpt/chat-gpt.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    ChatGPTModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
