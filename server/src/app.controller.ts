import { Body, Controller, Get, HttpCode, HttpStatus, Post } from '@nestjs/common'

import { AppService } from 'src/app.service'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {} 

  @Get('health')
  @HttpCode(HttpStatus.OK)
  check() {
      return 'OK'
  }

  @Post('wtw')
  @HttpCode(HttpStatus.OK)
  generateWtw(@Body('prompt') prompt: string) {
    return this.appService.getWtw(prompt)
  }

  @Post('wtd')
  @HttpCode(HttpStatus.OK)
  generateWtd(@Body('prompt') prompt: string, @Body('region') region: string = '', @Body('timeframe') timeframe: string) {
    return this.appService.getWtd(prompt, region, timeframe)
  }
}
