import { useCallback, useEffect, useState } from 'react'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import ErrorModal from './components/ErrorModal'
import ForecastPage from './ForecastPage'
import Loader from './components/Loader'
import TimePage from './TimePage'
import Weather, {
  handleWeatherApiError,
  WeatherErrorDetails,
} from './resources/Weather'
import { Forecast, BDCLocation } from './resources/interfaces'
import { darkTheme, GlobalStyles, lightTheme } from './theme'

function App() {
  // Theming
  const [theme, setTheme] = useState('light')
  const isDarkTheme = theme === 'dark'
  const toggleTheme = useCallback(() => {
    const updatedTheme = isDarkTheme ? 'light' : 'dark'
    setTheme(updatedTheme)
    localStorage.setItem('dw-theme', updatedTheme)
  }, [isDarkTheme, setTheme])

  useEffect(() => {
    const savedTheme = localStorage.getItem('dw-theme')
    const prefersDark =
      window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: dark)').matches
    if (savedTheme && ['dark', 'light'].includes(savedTheme)) {
      setTheme(savedTheme)
    } else if (prefersDark) {
      setTheme('dark')
    }
  }, [setTheme])

  // State
  const [overallForecast, setOverallForecast] = useState<Forecast>()
  const [hourlyForecast, setHourlyForecast] = useState<Forecast>()
  const [location, setLocation] = useState<BDCLocation>()
  const [loading, setLoading] = useState(true)
  const [hasError, setHasError] = useState(false)
  const [error, setError] = useState<WeatherErrorDetails>({
    title: '',
    detail: '',
  })

  const setData = useCallback(async () => {
    // Get position
    let lat
    let long
    try {
      const {
        coords: { latitude, longitude },
      } = await Weather.getPosition()
      lat = latitude
      long = longitude
    } catch (e: any) {
      let { title, detail } = handleWeatherApiError(e)
      detail = 'Error getting position: ' + detail
      setError({ title, detail })
      setLoading(false)
      setHasError(true)
    }

    // Get location
    try {
      if (lat && long) {
        const loc = await Weather.getLocation(lat, long)
        if (loc) {
          setLocation(loc)
        }
      }
    } catch (e: any) {
      let { title, detail } = handleWeatherApiError(e)
      detail = 'Error getting location: ' + detail
      setError({ title, detail })
      setLoading(false)
      setHasError(true)
    }

    // Get forecast
    try {
      if (lat && long) {
        const forecasts = await Weather.getWeather(lat, long)
        if (forecasts) {
          const { overall, hourly } = forecasts
          setOverallForecast(overall)
          setHourlyForecast(hourly)
          setLoading(false)
        }
      }
    } catch (e: any) {
      let { title, detail } = handleWeatherApiError(e)
      detail = 'Error getting weather: ' + detail
      setError({ title, detail })
      setLoading(false)
      setHasError(true)
    }
  }, [setHourlyForecast, setLoading, setOverallForecast])

  // Set initial position and forecast
  useEffect(() => {
    setData()

    // Get forecast updates every 5 minutes
    const interval = setInterval(setData, 300000)
    return () => clearInterval(interval)
  }, [setData])

  return (
    <ThemeProvider theme={isDarkTheme ? darkTheme : lightTheme}>
      <GlobalStyles />
      {loading ? (
        <Loader />
      ) : hasError ? (
        <ErrorModal {...error} />
      ) : (
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Navigate to={'/overall'} />} />
            <Route
              path="/overall"
              element={
                overallForecast &&
                location && (
                  <ForecastPage
                    forecast={overallForecast}
                    location={location}
                    navigationPath={'/hourly'}
                    themeChange={toggleTheme}
                  />
                )
              }
            />
            <Route
              path="/hourly"
              element={
                hourlyForecast &&
                location && (
                  <ForecastPage
                    forecast={hourlyForecast}
                    location={location}
                    navigationPath={'/overall'}
                    themeChange={toggleTheme}
                  />
                )
              }
            />
            <Route path="/time" element={<TimePage />} />
          </Routes>
        </BrowserRouter>
      )}
    </ThemeProvider>
  )
}

export default App
