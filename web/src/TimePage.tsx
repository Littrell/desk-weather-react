import { useCallback, useEffect, useState } from "react"
import { format } from 'date-fns'
import styled from "styled-components"

const StyledTimeContainer = styled.div((_props) => ({
    display: 'flex',
    justifyContent: 'center',
    height: '100vh',
}))

const StyledTime = styled.div((_props) => ({
    fontSize: '3rem',
    position: 'absolute',
    textAlign: 'center',
    top: 'calc(50% - 3rem)',
}))

interface TimePageProps {}

const TimePage: React.FC<TimePageProps> = () => {

    const [localTime, setLocalTime] = useState('')
    const [dateString, setDateString] = useState('')

    const setTime = useCallback(() => {
        const date = new Date()
        setLocalTime(date.toLocaleTimeString())
        setDateString(format(date, 'MMMM do'))
    }, [])

    useEffect(() => {
        setTime()
        const interval = setInterval(setTime, 1000)
        return () => clearInterval(interval)
    }, [setTime])

    return (
        <StyledTimeContainer>
            <StyledTime>{dateString}<br />{localTime}</StyledTime>
        </StyledTimeContainer>
    )
}

export default TimePage