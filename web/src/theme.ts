import { createGlobalStyle } from "styled-components"

interface DefaultTheme {
    body: 'white'|'black';
    text: 'white'|'black'
}

export const GlobalStyles = createGlobalStyle<{ theme: DefaultTheme }>(({ theme }) => ({
    body: {
        background: theme.body,
        color: theme.text,
    }
}))

export const lightTheme = {
    body: 'white',
    text: 'black',
}

export const darkTheme = {
    body: 'black',
    text: 'white',
}