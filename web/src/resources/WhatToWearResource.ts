import { AxiosInstance } from 'axios'

import Resource from './resource'

export interface WhatToWearResponse {
    finish_reason: string
    index: number
    message: {
        content: string
        role: 'assistant'
    }
}

export interface WhatToWearRequests {
    getWhatToWear(detailedWeather: string): Promise<WhatToWearResponse[]>
    getWhatToDo(detailedWeather: string, region: string, timeframe: string): Promise<WhatToWearResponse[]>
    getHealth(): Promise<boolean>
}

// TODO rename me
const actions = (client: AxiosInstance): WhatToWearRequests => {
    const getWhatToWear = async (detailedWeather: string) => {
        return client.post('http://localhost:3000/wtw', { prompt: detailedWeather })
            .then((response) => response.data.filter((whatToWear: WhatToWearResponse) =>
                whatToWear.message.role === 'assistant'))
    }

    const getWhatToDo = async (detailedWeather: string, region: string, timeframe: string) => {
        return client.post('http://localhost:3000/wtd', { prompt: detailedWeather, region, timeframe })
            .then((response) => response.data.filter((resp: unknown) => {
                console.log(resp)
                return true
            }))
    }

    const getHealth = async () => {
        try {
            return client.get('http://localhost:3000/health').then(() => true)
        } catch (e) {
            return false
        }
    }

    return {
        getWhatToWear,
        getWhatToDo,
        getHealth,
    }
}

export default Resource(actions)