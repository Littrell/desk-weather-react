import axios, { AxiosError, AxiosInstance } from 'axios'
import { format } from 'date-fns'

import client from './client'
import { Forecast, Forecasts, BDCLocation, Routes } from './interfaces'
import Resource from './resource'

export interface WeatherRequests {
  getPosition(): Promise<GeolocationPosition>
  getWeather(lat: number, long: number): Promise<Forecasts>
  getLocation(lat: number, long: number): Promise<BDCLocation>
  ping(): Promise<boolean>
}

// TODO error handling

const actions = (client: AxiosInstance): WeatherRequests => {
  const getPosition: WeatherRequests['getPosition'] = () => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          resolve(position)
        },
        (error) => {
          reject(error)
        }
      )
    })
  }

  const getWeather: WeatherRequests['getWeather'] = async (
    lat: number,
    long: number
  ) => {
    const routes = await getWeatherRoutes(lat, long)

    const [overall, hourly] = await Promise.all([
      getForecast(routes.forecast),
      getForecast(routes.forecastHourly),
    ])

    hourly.periods = hourly.periods.map((period) => {
      return {
        ...period,
        name: format(new Date(period.startTime), 'iii, h aa'),
      }
    })

    return { overall, hourly }
  }

  const getLocation: WeatherRequests['getLocation'] = (latitude: number, longitude: number) => {
    return client.get('https://api.bigdatacloud.net/data/reverse-geocode-client', { params: { latitude, longitude }}).then((response) => response.data)
  }

  const ping: WeatherRequests['ping'] = () => {
    return client.get('https://api.weather.gov').then(({ data }) => {
      return data.status === 'OK'
    })
  }

  return {
    getLocation,
    getPosition,
    getWeather,
    ping,
  }
}

export default Resource(actions)

// Helpers

// TODO better types

const getForecast = (url: string): Promise<Forecast> => {
  return client.get(url).then((response) => {
    return response.data.properties
  })
}

const getWeatherRoutes = (lat: number, long: number): Promise<Routes> => {
  return client
    .get(`https://api.weather.gov/points/${lat},${long}`)
    .then((response) => {
      const { data } = response
      return {
        forecast: data.properties.forecast,
        forecastHourly: data.properties.forecastHourly,
        forecastZone: data.properties.forecastZone,
        fireWeatherZone: data.properties.fireWeatherZone,
      }
    })
}

export interface WeatherErrorDetails {
  title: string
  detail: string
}

export const handleWeatherApiError = (e: (Error|AxiosError)): WeatherErrorDetails => {
  if (axios.isAxiosError(e)) {
    const errorTypes = [
      'https://api.weather.gov/problems/InvalidPoint',
      'https://api.weather.gov/problems/UnexpectedProblem',
    ]
    if (e.response?.data?.type && errorTypes.includes(e.response?.data?.type)) {
      let { title, detail } = e.response.data
      title = 'Weather API Error: ' + title
      return { title, detail }
    } else {
      return {
        title: 'Weather API Error: Unhandled Axios Error',
        detail: JSON.stringify(e),
      }
    }
  } else {
      return {
        title: 'Weather API Error: Unhandled Error',
        detail: JSON.stringify(e),
      }
  }
}