import axios from 'axios'
import axiosRetry from 'axios-retry'

export type UnknownObject<Type = unknown> = Record<string, Type>

const client = axios.create()

axiosRetry(client, { retries: 3 })

export default client
