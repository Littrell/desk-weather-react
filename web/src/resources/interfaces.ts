export interface BDCLocation {
  latitude: number
  longitude: number
  continent: string
  lookupSource: string
  continentCode: string
  localityLanguageRequested: string
  city: string
  countryName: string
  countryCode: string
  postcode: string
  principalSubdivision: string
  principalSubdivisionCode: string
  plusCode: string
  locality: string
}

export interface Forecasts {
  overall: Forecast
  hourly: Forecast
}

export interface Forecast {
  elevation: Elevation
  forecastGenerator: string
  generatedAt: string
  hasError: boolean
  periods: Period[]
  units: string
  updatedTime: string
  updated: string
  validTime: string
}

export interface Elevation {
  value: number
  unitCode: string
}

export interface Period {
  number: number
  name: string
  startTime: string
  endTime: string
  isDayTime: boolean
  temperature: number
  temperatureUnit: string
  temperatureTrend: any
  windSpeed: string
  windDirection: string
  icon: string
  shortForecast: string
  detailedForecast: string
}

export interface Routes {
  forecast: string
  forecastHourly: string
  forecastZone: string
  fireWeatherZone: string
}
