import React from 'react'
import styled from 'styled-components'
import { Period as PeriodI } from '../resources/interfaces'

const InlineDiv = styled.div(() => ({
  display: 'inline-block',
}))

const StyledPeriod = styled.div(({ theme }) => ({
  borderRight: `1px solid ${theme.text} !important`,
  padding: '8px',
  '&:last-child': {
    borderRight: '0px',
  },
}))

const StyledHand = styled.div((_props) => ({
  fontSize: '2em',
  textAlign: 'center',
  whiteSpace: 'nowrap',
}))

interface PeriodItemProps {
  click: React.MouseEventHandler<HTMLDivElement>
  selected: boolean
}

const PeriodItem: React.FC<PeriodI & PeriodItemProps> = ({
  name,
  temperature,
  temperatureUnit,
  selected,
  click,
}) => {
  return (
    <InlineDiv>
      {selected && <StyledHand>&#9759;</StyledHand>}
      <StyledPeriod onClick={click}>
        <div>{name}</div>
        <InlineDiv>{temperature}</InlineDiv>
        <InlineDiv>{temperatureUnit}</InlineDiv>
      </StyledPeriod>
    </InlineDiv>
  )
}

export default PeriodItem
