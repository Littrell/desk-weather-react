import { ModalStyle, OverlayStyle, StyledModal, StyledModalText, StyledTimes } from './ModalStyles'
import { WhatToWearResponse } from '../resources/WhatToWearResource'

StyledModal.setAppElement('#root')

interface WhatToDoModalProps {
    modalIsOpen: boolean
    onClose: () => void
    whatToDo: WhatToWearResponse[]
}

// TODO consolidate down to one modal
const WhatToDoModal: React.FC<WhatToDoModalProps> = ({
    modalIsOpen,
    onClose,
    whatToDo: whatToWear,
}) => {
    return (
        <StyledModal
            isOpen={modalIsOpen}
            className='_'
            overlayClassName='_'
            contentElement={(props, children) => (
              <ModalStyle {...props}>{children}</ModalStyle>
            )}
            overlayElement={(props, contentElement) => (
              <OverlayStyle {...props}>{contentElement}</OverlayStyle>
            )}
        >
            <StyledTimes onClick={onClose}>&times;</StyledTimes>
            <StyledModalText>{whatToWear.map((wtw) => wtw.message.content)}</StyledModalText>
        </StyledModal>
    )
}

export default WhatToDoModal