import Modal from 'react-modal'
import styled from 'styled-components'
import { ModalStyle, OverlayStyle } from './ModalStyles'

Modal.setAppElement('#root')

const StyledTitle = styled.div((_props) => ({
  fontSize: '1.5em',
  marginTop: '10px',
}))

const StyledDeveloperMessage = styled.div((_props) => ({
  color: 'red',
  fontSize: '1.2rem',
  marginTop: '10px',
}))

const StyledDetail = styled.div((_props) => ({
  fontSize: '1.2rem',
  marginTop: '10px',
}))

export interface ErrorModalProps {
    title: string,
    detail: string,
}

const ErrorModal: React.FC<ErrorModalProps> = ({
    title,
    detail,
}) => {
  return (
    <Modal
      isOpen={true}
      className='_'
      overlayClassName='_'
      contentElement={(props, children) => (
        <ModalStyle {...props}>{children}</ModalStyle>
      )}
      overlayElement={(props, contentElement) => (
        <OverlayStyle {...props}>{contentElement}</OverlayStyle>
      )}
    >
      <StyledTitle>{title}</StyledTitle>
      <StyledDeveloperMessage>NOTICE: Most of the time a page refresh (or two) will get you around this issue. If the issue does persist, please contact the developer.</StyledDeveloperMessage>
      <StyledDetail>{detail}</StyledDetail>
    </Modal>
  )
}

export default ErrorModal
