import Modal from 'react-modal'
import styled from 'styled-components'

export const ModalStyle = styled.div((_props) => ({
  minHeight: '35rem',
  margin: '2rem',
  padding: '1.5rem 2.5rem',
  display: 'flex',
  flexDirection: 'column',
  borderRadius: '0.25rem',
}))

export const OverlayStyle = styled.div(({ theme }) => ({
  alignItems: 'center',
  backgroundColor: theme.body,
  bottom: 0,
  display: 'flex',
  justifyContent: 'center',
  left: 0,
  position: 'fixed',
  right: 0,
  top: 0,
  zIndex: 3500,
}))

export const StyledTimes = styled.div((_props) => ({
  fontSize: '2em',
  textAlign: 'right',
  width: '100%',
}))

export const StyledModalText = styled.div(() => ({
  fontSize: '1.5em',
  marginTop: '10px',
}))

export const StyledModal = styled(Modal)({
    outline: 'rgba(0, 0, 0, 0)',
    whiteSpace: 'pre-line',
})
