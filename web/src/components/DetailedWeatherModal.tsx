import { ModalStyle, OverlayStyle, StyledModal, StyledModalText, StyledTimes } from './ModalStyles'

StyledModal.setAppElement('#root')

interface DetailedWeatherModalProps {
  forecast: string
  modalIsOpen: boolean
  onClose: () => void
}

// TODO consolidate down to one modal
const DetailedWeatherModal: React.FC<DetailedWeatherModalProps> = ({
  forecast,
  modalIsOpen,
  onClose,
}) => {
  return (
    <StyledModal
      isOpen={modalIsOpen}
      className='_'
      overlayClassName='_'
      contentElement={(props, children) => (
        <ModalStyle {...props}>{children}</ModalStyle>
      )}
      overlayElement={(props, contentElement) => (
        <OverlayStyle {...props}>{contentElement}</OverlayStyle>
      )}
    >
      <StyledTimes onClick={onClose}>&times;</StyledTimes>
      <StyledModalText>{forecast}</StyledModalText>
    </StyledModal>
  )
}

export default DetailedWeatherModal
