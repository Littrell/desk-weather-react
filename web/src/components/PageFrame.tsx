import styled from 'styled-components'

const PageFrame = styled.div((_props) => ({
  display: 'flex',
  flexFlow: 'column',
  height: 'calc(100% - 90px)',
  paddingLeft: '32px',
}))

export default PageFrame
