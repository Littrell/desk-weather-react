import styled from 'styled-components'

const Value = styled.div((_props) => ({
  display: 'inline-block',
  fontSize: '10rem',
}))

const Unit = styled.div((_props) => ({
  display: 'inline-block',
  fontSize: '3em',
  left: '-47px',
  position: 'relative',
}))

interface TemperatureProps {
  value: string
  units: string
  click: React.MouseEventHandler<HTMLDivElement>
}

const Temperature: React.FC<TemperatureProps> = ({ value, units, click }) => {
  return (
    <div onClick={click}>
      <Value>{value ? value : '-'}&deg;</Value>
      <Unit>{units ? units : '-'}</Unit>
    </div>
  )
}

export default Temperature
