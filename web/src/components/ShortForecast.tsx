import styled from 'styled-components'

export const ShortForecastContainer = styled.div(() => ({
  fontSize: '1.5rem',
  height: '110px',
}))

export const ShortForecast = styled.div(() => ({}))

export const WhatToWear = styled.div(() => ({
  fontSize: '1.2rem',
}))

export  const WhatToDo = styled.div(() => ({
  fontSize: '1.2rem',
}))
