import styled from 'styled-components'

const StyledLoader = styled.div(({ theme }) => ({
  margin: 0,
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
}))

function Loader() {
    return (
        <StyledLoader>Loading...</StyledLoader>
    )
}

export default Loader