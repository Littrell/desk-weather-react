import styled from 'styled-components'
import { Period as PeriodI } from '../resources/interfaces'
import PeriodItem from './PeriodItem'

const StyledPeriods = styled.div((_props) => ({
  bottom: 0,
  overflowX: 'scroll',
  overflowY: 'hidden',
  position: 'fixed',
  whiteSpace: 'nowrap',
  width: '100%',
  '-webkit-overflow-scrolling': 'touch',
}))

interface PeriodsListProps {
  periods: PeriodI[]
  selectedIndex: number
  select: (index: number) => void
}

const PeriodsList: React.FC<PeriodsListProps> = ({
  periods,
  selectedIndex,
  select,
}) => {
  return (
    <StyledPeriods>
      {periods.length ? (
        periods.map((period, i) => {
          return (
            <PeriodItem
              key={`period-${i}`}
              click={() => select(i)}
              selected={i === selectedIndex}
              {...period}
            />
          )
        })
      ) : (
        <>no periods defined</>
      )}
    </StyledPeriods>
  )
}

export default PeriodsList
