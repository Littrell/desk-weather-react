import { useGesture } from '@use-gesture/react'
import { useCallback, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import DetailedWeatherModal from './components/DetailedWeatherModal'
import PageFrame from './components/PageFrame'
import PeriodsList from './components/PeriodsList'
import { ShortForecast, ShortForecastContainer, WhatToDo, WhatToWear } from './components/ShortForecast'
import Temperature from './components/Temperature'
import { Forecast, BDCLocation, Period } from './resources/interfaces'
import WhatToWearModal from './components/WhatToWearModal'
import WhatToWearResource, { WhatToWearResponse } from './resources/WhatToWearResource'
import WhatToDoModal from './components/WhatToDoModal'

interface ForecastPageProps {
  forecast: Forecast
  location: BDCLocation
  navigationPath: '/overall' | '/hourly'
  themeChange: () => void
}

const StyledDragBox = styled.div(() => ({
  flexGrow: 1,
}))

const ForecastPage: React.FC<ForecastPageProps> = ({
  forecast,
  location,
  navigationPath,
  themeChange,
}) => {
  const navigate = useNavigate()

  const [detailModalIsOpen, setDetailModalIsOpen] = useState(false)
  const [whatToWear, setWhatToWear] = useState<WhatToWearResponse[]>([])
  const [whatToDo, setWhatToDo] = useState<WhatToWearResponse[]>([])
  // TODO rename these vars. The server now handles other responsibilities
  const [whatToWearAvailable, setWhatToWearAvailable] = useState(false)
  const [whatToWearModalIsOpen, setWhatToWearModalIsOpen] = useState(false)
  const [whatToDoModalIsOpen, setWhatToDoModalIsOpen] = useState(false)
  const [selectedForecastPeriod, setSelectedForecastPeriod] = useState<Period>(
    forecast.periods[0]
  )
  const [selectedForecastPeriodIndex, setSelectedForecastPeriodIndex] =
    useState(0)

  const handleSelectedIndex = useCallback(
    (index: number) => {
      setSelectedForecastPeriod(forecast.periods[index])
      setSelectedForecastPeriodIndex(index)
    },
    [forecast]
  )

  const handleShortForecastClick = useCallback(() => {
    console.log(selectedForecastPeriod)
    if (selectedForecastPeriod.detailedForecast.length !== 0) {
      setDetailModalIsOpen(true)
    }
  }, [selectedForecastPeriod])

  const handleWhatToWearClick = useCallback(async () => {
    if (selectedForecastPeriod.detailedForecast.length !== 0) {
      const _whatToWear = await WhatToWearResource.getWhatToWear(selectedForecastPeriod.detailedForecast)
      setWhatToWear(_whatToWear)
      setWhatToWearModalIsOpen(true)
    }
  }, [setWhatToWearModalIsOpen, selectedForecastPeriod])

  const handleWhatToDoClick = useCallback(async () => {
    if (selectedForecastPeriod.detailedForecast.length !== 0 && location.city) {
      // TODO send it the selectedForecastPeriod.startTime and endTime
      const _whatToDo = await WhatToWearResource.getWhatToDo(selectedForecastPeriod.detailedForecast, location.city, selectedForecastPeriod.name)
      setWhatToDo(_whatToDo)
      setWhatToDoModalIsOpen(true)
    }
  }, [setWhatToDoModalIsOpen, selectedForecastPeriod, location])

  const bind = useGesture({
    onDragEnd: ({ event, offset: [x], direction: [dx] }) => {
      event.preventDefault()
      if (dx < 0) {
        navigate(navigationPath)
        handleSelectedIndex(selectedForecastPeriodIndex)
      }
    },
  })

  const checkWhatToWearAvailability = useCallback(async () => {
    const available = await WhatToWearResource.getHealth()
    setWhatToWearAvailable(available)
  }, [setWhatToWearAvailable])

  useEffect(() => {
    handleSelectedIndex(selectedForecastPeriodIndex)
    checkWhatToWearAvailability()
  }, [checkWhatToWearAvailability, handleSelectedIndex, selectedForecastPeriodIndex])

  return (
    <PageFrame>
      <Temperature
        value={selectedForecastPeriod.temperature.toString()}
        units={selectedForecastPeriod.temperatureUnit}
        click={themeChange}
      />
      {selectedForecastPeriod.shortForecast && (
        <ShortForecastContainer>
          <ShortForecast onClick={handleShortForecastClick}>
            {selectedForecastPeriod.shortForecast}
          </ShortForecast>
          {selectedForecastPeriod.detailedForecast.length !== 0 && whatToWearAvailable &&
            <>
              <WhatToWear onClick={handleWhatToWearClick}>What to wear?</WhatToWear>
              <WhatToDo onClick={handleWhatToDoClick}>What to do?</WhatToDo>
            </>
          }
        </ShortForecastContainer>
      )}
      <p>{location.city ? <>{location.city}</> : '-'}</p>
      {selectedForecastPeriod.name && <>{selectedForecastPeriod.name}</>}
      <StyledDragBox {...bind()}></StyledDragBox>
      <PeriodsList
        periods={forecast.periods.length ? forecast.periods : []}
        select={(index) => handleSelectedIndex(index)}
        selectedIndex={selectedForecastPeriodIndex}
      />
      <DetailedWeatherModal
        forecast={selectedForecastPeriod.detailedForecast}
        modalIsOpen={detailModalIsOpen}
        onClose={() => setDetailModalIsOpen(false)}
      />
      <WhatToWearModal
        whatToWear={whatToWear}
        modalIsOpen={whatToWearModalIsOpen}
        onClose={() => setWhatToWearModalIsOpen(false)}
      />
      <WhatToDoModal
        whatToDo={whatToDo}
        modalIsOpen={whatToDoModalIsOpen}
        onClose={() => setWhatToDoModalIsOpen(false)}
      />
    </PageFrame>
  )
}

export default ForecastPage
