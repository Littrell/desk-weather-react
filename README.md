# desk-weather

[Overall - https://weather.likemittrell.com/overall](https://weather.likemittrell.com/overall)

[Hourly - https://weather.likemittrell.com/hourly](https://weather.likemittrell.com/hourly)

Most weather apps are too bloated with ads. If you go to [weather.com](https://weather.com), try to find the weather. This weather app is just the weather, sourced from [https://api.weather.gov](https://api.weather.gov). It uses the browser location.

**Tip #1** - In the Overall view, tap the short weather description for a more in-depth description of the weather.

**Tip #2** - Swipe in the empty space to switch between /hourly and /overall.

**Tip #3** - Tap the temperature to switch between light and dark mode.

**Tip #4** - The interface refreshes data every 5 minutes. Use this as a smart mirror!

**Tip #5** - Navigate to /time to see the time
